#!/usr/bin/env python3
################################################################
#
# castep_md_reader
#   basic script that can read a CASTEP .md file in python
#
# This script reads a .md file (or set of .md files in a .gz)
# and extracts the positions, velocities, etc.
#
################################################################
#  Based on castep2force by Corwin Kuiper 2020
#################################################################

import argparse
import gzip
import sys

from collections import OrderedDict

from typing import Dict, List, IO, Union

def atomic_energy(hartree: str) -> float:
    #md file has energies in Hartree. To convert to eV multiply by 27.211386245988
    return float(hartree) * 1.0

def atomic_temperature(kBT: str) -> float:
    #md file has temperature in Hartree per atom. To convert to K multiply by 3.157750248E5
    return float(kBT) * 1.0

def atomic_length(bohr: str) -> float:
    #md file has lengths in Bohr. To convert to Ang multiply by 0.529177210903
    return float(bohr) * 1.0

def atomic_velocity(velocity: str) -> float:
    #md file has velocity in Bohr per atomic-unit-of-time. Need to convert
    return float(velocity) * 1.0

def atomic_force(force: str) -> float:
    #md file has forces in Hartree per Bohr. To convert to eV/Ang multiply by 51.42206747632588536622
        return float(force) * 1.0

def atomic_stress(stress: str) -> float:
    #md file has stress in Harree per Bohr^3. To convert to eV/Ang^3 multiply by 183.63153644969497346643
    return float(stress) * 1.0


def fatal(message: str) -> None:
    if message.endswith("\n"):
        sys.stderr.write("FATAL: {}".format(message))
    else:
        sys.stderr.write("FATAL: {}\n".format(message))
    sys.exit(1)


def warning(message: str) -> None:
    if message.endswith("\n"):
        sys.stderr.write("WARN: {}".format(message))
    else:
        sys.stderr.write("WARN: {}\n".format(message))


class Atom:
    def set_position(self, x: float, y: float, z: float) -> None:
        self.x = x
        self.y = y
        self.z = z

    def set_velocity(self, x: float, y: float, z: float) -> None:
        self.v_x = x
        self.v_y = y
        self.v_z = z

    def set_force(self, x: float, y: float, z: float) -> None:
        self.f_x = x
        self.f_y = y
        self.f_z = z
    
    def __init__(self, atom_type: int) -> None:
        self.type = atom_type

    def to_string(self) -> str:
        #return "{} {:23.16E} {:23.16E} {:23.16E} {:23.16E} {:23.16E} {:23.16E} {:23.16E} {:23.16E} {:23.16E}".format(
        return "{} {:13.6F} {:13.6F} {:13.6F} {:13.6F} {:13.6F} {:13.6F} {:13.6F} {:13.6F} {:13.6F}".format(
            self.type, self.x, self.y, self.z, self.v_x, self.v_y, self.v_z, self.f_x, self.f_y, self.f_z
        )


class Box:
    def __init__(self, x: float, y: float, z: float) -> None:
        self.x = x
        self.y = y
        self.z = z

    def to_string(self) -> str:
        #return "{:23.16E} {:23.16E} {:23.16E}".format(self.x, self.y, self.z)
        return "{:13.6F} {:13.6F} {:13.6F}".format(self.x, self.y, self.z)


class Configuration:
    def __init__(self) -> None:
        # For python versions less than 3.7, need to explicitly use an ordered
        # dictionary as the order in must equal the order out.
        self.types_of_element = OrderedDict()  # type: Dict[str, int]
        self.boxes = []  # type: List[Box]
        self.energy = None  # type: Union[None, float]
        self.temperature = None  # type: Union[None, float]

        self.stress = []  # type: List[List[float]]

        # A dictionary where the key is a string representing an element and
        # contains a list of atoms of that type.
        self.body = OrderedDict()  # type: Dict[str, List[Atom]]

    def to_string(self) -> str:
        # Calculate number of atoms in the configuration
        number_of_atoms = 0
        for _, t in self.body.items():
            number_of_atoms += len(t)

        s = []
        # Example output - write configurations in suitable format for potfit
        s.append("#N {} 1".format(number_of_atoms))
        s.append("#C {}".format(" ".join(self.types_of_element.keys())))
        s.append("#X {}".format(self.boxes[0].to_string()))
        s.append("#Y {}".format(self.boxes[1].to_string()))
        s.append("#Z {}".format(self.boxes[2].to_string()))
        s.append("#E {:23.16E}".format(self.energy if self.energy else 0))
        #NB potfit does not care about temperature so don't output that here

        # Only output stresses if they are in the input file
        if len(self.stress) == 3:
            # #S stress_xx stress_yy stress_zz stress_xy stress_yz stress_xz
            s.append(
                "#S {:23.16E} {:23.16E} {:23.16E} {:23.16E} {:23.16E} {:23.16E}".format(
                    self.stress[0][0],
                    self.stress[1][1],
                    self.stress[2][2],
                    self.stress[0][1],
                    self.stress[1][2],
                    self.stress[0][2],
                )
            )
        s.append("#F")
        # End of header
        # Start of body
        for _, value in self.body.items():
            for atom in value:
                s.append(atom.to_string())
        # End with a newline
        s.append("")
        return "\n".join(s)


# Reads a configuration from specified file and returns a configuration object
# or None if at EOF
def read_configuration(file: IO[str]):
    
    config = Configuration()
    
    energy = None
    temperature = None
    
    for line in file:
        stripped_line = line.strip()
        
        
        # Skip over the header
        if line.strip() == "BEGIN header":
            #print('Header skipping')
            for _ in range(4):
                #print('Header skipped')
                file.readline()
                
            continue

        # reached empty line between md steps
        if stripped_line == "":
           # print('Reached empty line between md steps')
            break

        split_line = stripped_line.split()
        tag = split_line[len(split_line) - 1]
        
        # Stress matrix element
        if tag == "S":
            x = atomic_stress(split_line[0])
            y = atomic_stress(split_line[1])
            z = atomic_stress(split_line[2])

            config.stress.append([x, y, z])

        if tag == "E":
            # energy, has 3 elements in it, Total, Hamiltonian, Kinetic.
            # In most MD ensembles we are interested in the total energy
            config.energy = atomic_energy(split_line[0]) #total E is the first entry

        if tag == "T":
            # temperature
            config.temperature = atomic_temperature(split_line[0])

        # Cell matrix vector
        if tag == "h":
            x = atomic_length(split_line[0])
            y = atomic_length(split_line[1])
            z = atomic_length(split_line[2])

            config.boxes.append(Box(x, y, z))

        # coordinate of atom
        if tag == "R":
            element_type = split_line[0]
            atom_number = int(split_line[1])
            x = atomic_length(split_line[2])
            y = atomic_length(split_line[3])
            z = atomic_length(split_line[4])

            # add atom type, eg Ar, to the list of types of element
            if element_type not in config.types_of_element:
                config.types_of_element[element_type] = len(config.types_of_element)
                config.body[element_type] = []
            element_type_int = config.types_of_element[element_type]

            a = Atom(element_type_int)
            a.set_position(x, y, z)
            config.body[element_type].append(a)

        # velocity of atom
        if tag == "V":
            element_type = split_line[0]
            atom_number = int(split_line[1])
            v_x = atomic_velocity(split_line[2])
            v_y = atomic_velocity(split_line[3])
            v_z = atomic_velocity(split_line[4])
            
            # atom numbers in md file start at 1
            atom_index = atom_number - 1
            config.body[element_type][atom_index].set_velocity(v_x, v_y, v_z)
                        
        # force acting on atom
        if tag == "F":
            element_type = split_line[0]
            atom_number = int(split_line[1])
            f_x = atomic_force(split_line[2])
            f_y = atomic_force(split_line[3])
            f_z = atomic_force(split_line[4])

            # atom numbers in md file start at 1
            atom_index = atom_number - 1
            config.body[element_type][atom_index].set_force(f_x, f_y, f_z)
            
    else:
        # test for whether file is at at EOF. else runs if for loop didn't
        # encounter break, which happens when at EOF.
        return None
    
    return config


def parse_cli_flags(argv: List[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Read a CASTEP molecular dynamics trajectory file (or set of files)",
        epilog="Example usage: ./castep_md_reader -o configuration.out mdinput.md",
    )

    parser.add_argument(
        "files", type=str, nargs="+", help="CASTEP molecular dynamics trajectory file",
    )
    
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        required=False,
        help="Output file to write, Default is to standard output",
        default="-",
    )

    parser.add_argument(
        "-f",
        "--final",
        action="store_true",
        help="Only use the final configuration in each file",
    )

    return parser.parse_args(argv)



def open_file(filename: str) -> IO[str]:
    if filename == "-":
        return sys.stdin
    if filename.endswith(".gz"):
        return gzip.open(filename, "rt")
    return open(filename, "rt")


def read_md_file(filename: str) -> List[Configuration]:

    # CASTEP produces a .md files with multiple iteration steps, each one is a different configuration
    configurations = []
    i=0

    try:
        with open_file(filename) as file:
            # continually read configurations until read configuration returns
            # false, at which point file has reached eof
            while True:
                config = read_configuration(file)
                if config is None:
                    break
                else:
                    print('Reading config',i)
                configurations.append(config)
                i=i+1
                
    except FileNotFoundError:
        fatal("Cannot read file '{}' as it does not exist".format(filename))

    return configurations


def write_configurations(configurations: List[Configuration], output: str) -> None:
    # '-' often means standard input / standard output, so I have used it in
    # this software for that purpose.
    if output == "-" or output is None:
        for conf in configurations:
            sys.stdout.write(conf.to_string())
    else:
        with open(output, "w") as file:
            for conf in configurations:
                file.write(conf.to_string())


def main(argv: List[str]) -> None:
    arguments = parse_cli_flags(argv)
    filenames = arguments.files


    configurations = []  # type: List[Configuration]
    for filename in filenames:
        configuration = read_md_file(filename)
        if arguments.final:
            # add only the last configuration
            configurations.append(configuration[-1])
        else:
            # add all configurations
            configurations.extend(configuration)

    output_filename = arguments.output
    write_configurations(configurations, output_filename)


if __name__ == "__main__":
    main(sys.argv[1:])
    
